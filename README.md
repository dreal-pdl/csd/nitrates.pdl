
<!-- README.md is generated from README.Rmd. Please edit that file -->

# nitrates.pdl

Ce projet contient le code de l’application shiny golem de valorisation
des mesures de présence de nitrates dans les cours d’eau et les nappes
des Pays de la Loire.

- le répertoire **data-raw** contient les scripts R de chargement et de
  préparation des données ;

- le répertoire **R** contient le code des fonctions et des modules de
  l’application Shiny.

- l’application se trouve à l’adresse suivante :
  <https://ssm-ecologie.shinyapps.io/nitrates/>
