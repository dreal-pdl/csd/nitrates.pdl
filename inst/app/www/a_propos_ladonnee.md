Les données sont issues :

-   pour les **eaux de surface** (appelées **ESU**) : de la base Naïades (essentiellement alimentée par des données de l'Agence de l'eau Loire-Bretagne (AELB)) et des données de l'Agence régionale de santé (ARS) des Pays de la Loire ;

-   pour les **eaux souterraines** (appelées **ESO**) : de la base ADES gérée par le BRGM, complétées par des données de l'ARS Pays de la Loire.

Les données ADES et Naïades sont disponibles en open data sur les sites correspondants :

-   <https://ades.eaufrance.fr/>

-   <http://www.naiades.eaufrance.fr/>

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure (mise à jour annuelle des données).

<img src="photo_riviere_labo_hydro.jpg" style="float:right;margin:5px 20px 5px 0px" width="40%"/>

*crédits photo : Laboratoire d'hydrobiologie de la DREAL des Pays de la Loire*
