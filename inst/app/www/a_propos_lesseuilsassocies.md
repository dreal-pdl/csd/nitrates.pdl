Pour le suivi des nitrates dans l'eau plusieurs seuils sont utilisés et correspondent à des objectifs différents. Les P90 sont ainsi représentés par classe de couleur intégrant les bornes 18 mg/L, 40mg/L et 50mg/L notamment. Ces bornes sont légèrement différentes du classement « seq'eau ».

**Limites « eau potable »** :

-   **limite de qualité dans l'eau brute** destinée à la consommation humaine (arrêté du 11 janvier 2007 modifié, en application de la directive 98/83/CE) : ESO : 100 mg/L et ESU : 50 mg/L ;

-   **limite de qualité dans l'eau traitée** destinée à la consommation humaine : ESO et ESU : 50 mg/L.

**Limite au sens de la DCE** : seuil de 50mg/L pour les ESU, la masse d'eau risque de ne pas atteindre le bon état écologique.

**Seuils utilisés pour la définition des zones vulnérables** (notamment) :

-   ESU : 18mg/L

-   ESO : 50 mg/L
