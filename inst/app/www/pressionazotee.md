**Origine des données**

Les données de pressions azotées sont issues des données collectées depuis la campagne culturale 2017-2018 dans le cadre du dispositif de télédéclaration des pratiques de fertilisation azotée mis en place en 2018 pour évaluer le Programme d'actions régional Nitrates (PAR).

**Exploitants concernés**

Cette télédéclaration est obligatoire pour toutes les exploitations ayant leur siège en Pays de la Loire, à l'exception depuis 2024, des exploitations ayant moins de 4 ha et moins de 4 équivalents UGB.

Les exploitations en maraîchage, arboriculture et viticulture sont concernées par cette déclaration quelle que soit leur surface.

**Modalités de déclaration**

Dans ce dispositif, deux modalités de transmission des données sont proposées aux exploitants :

-   en autonomie via une plateforme internet ;

-   en mandatant un prestataire de service qui peut transmettre les données groupées par fichier.

**Données collectées**

Les données recueillies comprennent notamment les quantités d'azote (minéral et organique) épandues (en kg) à l'échelle de l'ensemble de l'exploitation et pour la campagne culturale considérée ainsi que la SAU de l'exploitation.

La quantité d'azote organique épandue est établie à partir de la quantité d'azote produite à partir des animaux de l'exploitation, la quantité d'azote organique non maîtrisable produite, la quantité d'azote organique cédée à un tiers et la quantité d'azote organique importée sur l'exploitation.

Pour une exploitation à cheval sur plusieurs régions, ces quantités sont pondérées en fonction de la SAU hors Pays de la Loire, afin d'obtenir la quantité d'azote épandue en Pays de la Loire.

**Contrôles de cohérence**

Pour assurer la fiabilité des résultats statistiques, des contrôles de cohérence ont été appliqués aux données déclarées afin d'identifier les déclarations comportant des incohérences dans les données (erreurs de saisie, données manquantes, erreurs de calcul...).

Les déclarations estimées incohérentes ont été écartées pour éviter les biais statistiques.

**Méthode d'agrégation des données à l'échelle de la masse d'eau, des départements et de la région**

Les données de quantité d'azote et de SAU déclarées par chaque exploitation sont rattachées à la commune du siège de l'exploitation.

Elles sont ensuite ramenées à l'échelle des masses d'eau en prenant la part de la superficie de chaque commune dans la masse d'eau.

La quantité totale calculée pour la masse d'eau est ensuite divisée par la surface de SAU de la masse d'eau ayant fait l'objet d'une déclaration pour obtenir la pression azotée exprimée en kg d'azote par ha.

Pour le calcul de la pression azotée à l'échelle des départements et de la région, la quantité totale d'azote déclarée à l'échelle du département ou de la région est ramenée à la surface ayant fait l'objet d'une déclaration dans le département ou la région.

**Pour en savoir plus**

Sur le site internet de la DRAAF Pays de la Loire, une page est consacrée à la déclaration des pratiques de fertilisation azotée : <https://draaf.pays-de-la-loire.agriculture.gouv.fr/declaration-des-pratiques-de-fertilisation-azotee-r512.html>
