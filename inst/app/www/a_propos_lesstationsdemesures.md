Les stations de mesures des nitrates où sont effectués les prélèvements appartiennent à différents réseaux :

-   le **suivi des milieux** au titre de la directive cadre sur l'eau (DCE), comptant aussi pour le suivi de la directive nitrate (agence de l'eau et éventuels réseaux locaux), destiné à connaître l'état général des cours d'eau et nappes souterraines ;

-   le **suivi des captages d'eau** destinés à la consommation humaine (ARS), destiné principalement au contrôle de la teneur en nitrates vis-à-vis de la production d'eau potable, dans les eaux « brutes » (non traitées).

Parmi ce dernier réseau, sont identifiés :

-   les **captages « eau potables »**, les captages dont l'usage est soit l'alimentation en eau potable, soit destiné à l'industrie agro-alimentaire, soit des usages privés ;

-   les **47 captages prioritaires** de la région, dont 45 figurent dans le bassin Loire-Bretagne et donc dans le SDAGE éponyme.

Les données peuvent être représentées :

-   à l'**échelle régionale**, du **SAGE** (mais ne seront pas intégrées les stations situées hors région) ou du **bassin versant de masse d'eau**, pour les points de mesure superficiels ;

-   à l'**échelle régionale** pour les points de mesure souterrains.

<img src="2017_SAGE_PDL.png" style="margin:5px 20px 5px 0px" width="75%"/>
