Une mesure est retenue si elle dépasse le seuil de détection et reste inférieur au seuil de saturation. Les concentrations situées en-dessous du seuil de quantification (0,5 mg/L pour les laboratoires agréés) sont tout de même retenues et une valeur forfaitaire leur est attribuée.

Afin de traduire l'ensemble des mesures sur une année, c'est le percentile 90 (ou P90) qui est utilisé (selon méthode de l'arrêté du 5 mai 2015). La règle du percentile 90 consiste à prendre en compte la valeur en deçà de laquelle se situent 90 % des mesures réalisées au cours de l'année considérée. Mais lorsque dix mesures ou moins ont été réalisées au total lors de la campagne, la teneur en nitrates retenue est la valeur maximale.

Ainsi:

-   si l'on dispose de moins de 10 valeurs (strict), la valeur maximale sera retenue ;

-   si l'on dispose de 10 à 20 valeurs, la 2ème valeur maximale sera retenue ;

-   si l'on dispose de 20 à 30 valeurs, la 3ème valeur maximale... etc

Exemple :

<img src="exemple_P90.png" style="float: left;margin:5px 20px 5px 0px" width="100%"/>

Les **moyennes** ne sont pas utilisées, car elles peuvent être tirées par les extrêmes. Les **médianes** ne représentent pas non plus assez justement le risque de valeur hautes qu'il convient de prendre en compte.

A l'échelle de la masse d'eau (bassin versant), deux paramètres sont calculés:

- la **moyenne des P90** des stations dans le périmètre de la masse d'eau ;

- la **valeur maximale** de P90 retrouvée dans le périmètre de la masse d'eau.

Ces deux paramètres se retrouvent dans l'onglet "Eaux superficielles bassin versant" de l'application.

Enfin, une **tendance** est calculée uniquement si la station dispose d'un P90 sur 5 années consécutives dans une période de 10 ans. Elle est calculée pour 2016 (période 2007-2016), pour 2017 (période 2008-2017) et pour 2018 (période 2009-2018).

Si cette condition est remplie, on distingue alors les cas suivants :

-   **pas de tendance** : les P90 ont oscillé de plus de 5mg/l (pour une moyenne proche des 50 mg/l) et de plus de 3 mg/l (pour une moyenne autour de 10 mg/l) autour de la moyenne sur la période ou bien le coefficient de détermination de la droite de régression linéaire \<0,4 ;

-   **stabilité** : les P90 sont restés proches de la moyenne dans la fourchette indiquée ci avant ;

-   **hausse ou baisse** : le coefficient de détermination de la droite de régression linéaire montre une faible dispersion des P90 autour de celle-ci (coefficient retenu = 0,4), la variation est au moins de l'ordre du mg/l par an.
