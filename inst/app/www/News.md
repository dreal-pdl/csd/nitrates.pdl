# 

Annonce des évolutions de l'application et des données qu'elle valorise.

### Nitrates v3

Publiée le xx février 2024

##### Evolutions de l'application

- Ajout d'une page Pression azotée

### Nitrates v2.3

Publiée le 7 décembre 2023

##### Evolution des données

-   Ajout des données 2022

##### Evolutions de l'application

-   L'application utilise  le [Système de Design de l'Etat (DSFR)](https://www.systeme-de-design.gouv.fr/). Le codage s'appuie sur le package [{shinygouv}](https://github.com/spyrales/shinygouv).

### Nitrates v2.2

Publiée le 7 décembre 2022

##### Evolution des données

-   Ajout des données 2021

### Nitrates v2.1

Publiée le 23 novembre 2021

##### Evolution des données

-   Ajout des données 2020

##### Evolutions de l'application

-   Amélioration de la collecte et de la modélisation des données

### Nitrates v2

Publiée le 2 décembre 2020

##### Evolution des données

-   Ajout des données 2019

##### Evolutions de l'application

-   Ajout d'un bouton permettant d'afficher les cartes en plein écran
-   Ajout d'un bouton de téléchargement (png, jpeg, pdf et csv) aux diagrammes

### Nitrates v1

Publiée le 6 décembre 2019
