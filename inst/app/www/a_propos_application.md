**Cette application met à disposition les données sur les nitrates dans les cours d'eau et les nappes souterraines de la région Pays de la Loire à partir de 2007**.

La lutte contre la pollution diffuse par les nitrates est un enjeu important. Des concentrations en nitrates en excès dans l'eau peuvent la rendre impropre à la consommation humaine ou induire des problèmes d'eutrophisation. Son suivi dans les eaux superficielles (cours d'eau) et souterraines (nappes) est à la fois une obligation européenne et nationale.

La présente application n'aborde pas la problématique de l'origine de la présence des nitrates dans les eaux analysées. Elle livre les mesures et des indicateurs associés sans chercher à établir des corrélations ou des liens de causalité entre la concentration mesurée et les pratiques associées.

Les concentrations en nitrates sont disponibles par station ou regroupées à la masse d'eau pour les eaux superficielles, et par station pour les eaux souterraines.

*Attention ! Les stations prisent en compte pour les calculs sont celles qui se situent sur le territoire de la région des Pays de la Loire. Pour un SAGE à cheval sur les Pays de la Loire et une autre région, les mesures des stations de l'autre région ne participent pas aux calculs.*

Des valorisations graphiques vous sont proposées. L'ensemble des données peut également être téléchargé dans la rubrique « Télécharger les données ».

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.

N'hésitez pas à [nous contacter](mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr?subject=Application%20nitrates&body=Bonjour,%22) pour toute remarque ou suggestion.

<img src="carte_region_esu_station.png" style="float:right;margin:5px 20px 5px 0px" width="40%"/>
