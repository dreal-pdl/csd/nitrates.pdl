#' concentration_nitrates_eso UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @import shinygouv
#' @importFrom highcharter highchartOutput
#' @importFrom leaflet leafletOutput
mod_concentration_nitrates_eso_ui <- function(id){
  ns <- NS(id)
  tagList(
    # pour afficher un bouton de telechargement dans les cartes leaflet
    tags$script(src = "https://rawgit.com/rowanwins/leaflet-easyPrint/gh-pages/dist/bundle.js"),
    fluidRow_dsfr(
      column_dsfr(
        width = 6,
        # "carte classe P90 par station"
        leaflet::leafletOutput(ns("carte_P90_station_eso"))
      ),
      column_dsfr(
        width = 2,
        # "graphe de r\u00e9partition des P90"
        highcharter::highchartOutput(ns("donut_P90_station_eso_hc"))
      ),
      column_dsfr(
        width = 4,
        # "graphe nombre de stations par classe de P90"
        highcharter::highchartOutput(ns("col_P90_station_eso_hc"))
      )
    ),
    fluidRow_dsfr(
      column_dsfr(
        width = 6,
        # "graphe de pourcentage des mesures sup\u00e9rieures ou \u00e9gales \u00e0 18 mg/l"
        highcharter::highchartOutput(ns("bar_seuil_eso_hc"))
      ),
      column_dsfr(
        width = 6,
        # "graphe de r\u00e9partition des P90 par ann\u00e9e"
        highcharter::highchartOutput(ns("colonnes_P90_station_eso_hc"))
      )
    )
  )
}

#' concentration_nitrates_eso Server Functions
#'
#' @noRd
#' @importFrom highcharter renderHighchart
#' @importFrom leaflet renderLeaflet
#' @importFrom shiny reactiveValues
#' @importFrom shinygouv withSpinner_dsfr
mod_concentration_nitrates_eso_server <- function(id, global, CodeNatureEau){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    locale <- shiny::reactiveValues()

    observeEvent(c(global$station_eso_SelectAnnee, global$station_eso_SelectSage, global$station_eso_SelectBV),{
      shinygouv::withSpinner_dsfr({
        locale$bassin_versant <- bassin_versant_fct(global$station_eso_SelectSage)
        locale$station <- station_fct(global$station_eso_SelectBV,
                                      locale$bassin_versant)
        locale$nitrates_P90_carte  <- nitrates_P90_carte_fct(global$station_eso_SelectAnnee,
                                                             CodeNatureEau = CodeNatureEau,
                                                             locale$station)
        locale$sage_carte <- sage_carte_fct(global$station_eso_SelectSage)
        locale$bassin_versant_station_carte <- bassin_versant_station_carte_fct(global$station_eso_SelectBV,
                                                                                global$station_eso_SelectSage)
        locale$cadre_carte_station <- cadre_carte_station_fct(global$station_eso_SelectSage,
                                                              global$station_eso_SelectBV,
                                                              locale$bassin_versant_station_carte)
        locale$carte_P90_station <- carte_P90_station_fct(global$station_eso_SelectSage,
                                                          global$station_eso_SelectBV,
                                                          locale$nitrates_P90_carte,
                                                          locale$sage_carte,
                                                          locale$bassin_versant_station_carte,
                                                          locale$cadre_carte_station)

        locale$repartition_P90_station_donut <- repartition_P90_station_donut_fct(global$station_eso_SelectSage,
                                                                                  global$station_eso_SelectBV,
                                                                                  global$station_eso_SelectAnnee,
                                                                                  CodeNatureEau = CodeNatureEau)
        locale$couleur_repartition_P90_donut <- couleur_repartition_P90_donut_fct(locale$repartition_P90_station_donut)
        locale$donut_P90_station_hc <- donut_P90_station_hc_fct(locale$repartition_P90_station_donut,
                                                                global$station_eso_SelectAnnee,
                                                                locale$couleur_repartition_P90_donut)

        locale$repartition_P90_station_col <- repartition_P90_station_col_fct(global$station_eso_SelectSage,
                                                                              global$station_eso_SelectBV,
                                                                              global$station_eso_SelectAnnee,
                                                                              CodeNatureEau = CodeNatureEau)
        locale$categorie_repartition_P90_col <- categorie_repartition_P90_col_fct(locale$repartition_P90_station_col)
        locale$col_P90_station_hc <- col_P90_station_hc_fct(locale$repartition_P90_station_col,
                                                            locale$categorie_repartition_P90_col,
                                                            global$station_eso_SelectAnnee)

        locale$depassement_seuil <- depassement_seuil_fct(CodeNatureEau = CodeNatureEau)
        locale$depassement_seuil_station <- depassement_seuil_station_fct(global$station_eso_SelectSage,
                                                                          global$station_eso_SelectBV,
                                                                          locale$depassement_seuil)
        locale$bar_seuil_hc <- bar_seuil_hc_fct(locale$depassement_seuil_station, seuil = 50)

        locale$repartition_P90_station_colonnes <- repartition_P90_station_colonnes_fct(global$station_eso_SelectSage,
                                                                                        global$station_eso_SelectBV,
                                                                                        CodeNatureEau = CodeNatureEau)
        locale$couleur_repartition_P90_colonnes <- couleur_repartition_P90_colonnes_fct(locale$repartition_P90_station_colonnes)
        locale$colonnes_P90_station_hc <- colonnes_P90_station_hc_fct(locale$repartition_P90_station_colonnes,
                                                                      locale$couleur_repartition_P90_colonnes)
      })
    })

    output$carte_P90_station_eso <- leaflet::renderLeaflet(locale$carte_P90_station)
    output$donut_P90_station_eso_hc <- highcharter::renderHighchart(locale$donut_P90_station_hc)
    output$col_P90_station_eso_hc <- highcharter::renderHighchart(locale$col_P90_station_hc)
    output$bar_seuil_eso_hc <- highcharter::renderHighchart(locale$bar_seuil_hc)
    output$colonnes_P90_station_eso_hc <- highcharter::renderHighchart(locale$colonnes_P90_station_hc)

  })
}

## To be copied in the UI
# mod_concentration_nitrates_eso_ui("concentration_nitrates_eso_1")

## To be copied in the server
# mod_concentration_nitrates_eso_server("concentration_nitrates_eso_1")
