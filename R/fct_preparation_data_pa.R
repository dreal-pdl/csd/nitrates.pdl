#' bassin_versant_pa_fct
#'
#' @description Une fonction pour produire une liste de codes de bassin versant à partir de la sélection d'un SAGE
#' @param pa_SelectSage sélection d'un SAGE
#'
#' @return Une liste de codes.
#'
#' @export
#' @importFrom dplyr pull filter
bassin_versant_pa_fct <- function(pa_SelectSage){
  if(pa_SelectSage == "Tous")({
    bassin_versant <- correspondance_sage_bassin_versant %>%
      dplyr::pull(code_bassin_versant) %>%
      unique()
  })
  else({
    bassin_versant <- correspondance_sage_bassin_versant %>%
      dplyr::filter(nom_sage == pa_SelectSage) %>%
      dplyr::pull(code_bassin_versant) %>%
      unique()
  })
}

#' carte_pa_fct
#'
#' @description Une fonction pour produire la carte de la pression azotée globale par bassin versant
#' @param campagne sélection d'une campagne culturale
#' @param pa_SelectSage sélection d'un SAGE
#'
#' @return Une carte.
#'
#' @export
#' @importFrom dplyr filter
#' @importFrom htmltools HTML
#' @importFrom htmlwidgets onRender
#' @importFrom leafem addHomeButton
#' @importFrom leaflet colorFactor leaflet leafletOptions addProviderTiles addPolygons labelOptions addLegend fitBounds
#' @importFrom leaflet.extras addFullscreenControl
carte_pa_fct <- function(campagne, pa_SelectSage){

  titre_export <- "carte_pression_azotee_bv"
  js_export_carte <- paste0("function(el, x) {
            L.easyPrint({
              sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
              filename: '", titre_export, "',
              exportOnly: true,
              hideControlContainer: true
            }).addTo(this);
            }")

  liste_bassin_versant <- bassin_versant_pa_fct(pa_SelectSage)

  filtered <- bassin_versant_carte_pa %>%
    dplyr::filter(
      annee_declaration == campagne,
      code_bassin_versant %in% liste_bassin_versant
      )
  factpal_bassin_versant <- leaflet::colorFactor(
    palette = palette_pa,
    filtered$classe,
    reverse = TRUE
  )
  labels <- sprintf(
    "%s",
    filtered$nom_bassin_versant
  ) %>%
    lapply(htmltools::HTML)

  sage_carte <- sage_carte_fct(pa_SelectSage)

  cadre_carte_bassin_versant <- cadre_carte_bassin_versant_fct(
    pa_SelectSage,
    sage_carte
  )

  if(pa_SelectSage == "Tous"){
    leaflet::leaflet(
      filtered,
      options = leaflet::leafletOptions(zoomControl = FALSE)
    ) %>%
      leaflet::addProviderTiles(provider = "CartoDB.Positron") %>%
      leaflet::addPolygons(
        data = n_region_exp_r52,
        group = "zoomer sur la r\u00e9gion",
        color = "black",
        weight = 1,
        smoothFactor = 0.5,
        opacity = 1.0,
        fillOpacity = 0
      ) %>%
      leaflet::addPolygons(
        data = filtered,
        fillColor = ~ factpal_bassin_versant(classe),
        weight = 3,
        opacity = 0.1,
        color = "white",
        dashArray = "3",
        fillOpacity = 0.8,
        label = labels,
        labelOptions = leaflet::labelOptions(
          style = list("font-weight" = "normal", padding = "3px 8px"),
          textsize = "10px",
          direction = "auto"
        )
      ) %>%
      leaflet::addPolygons(
        data = n_departement_exp_r52,
        color = "black",
        weight = 1,
        smoothFactor = 0.5,
        opacity = 1.0,
        fillOpacity = 0
      ) %>%
      leaflet::addPolygons(
        data = n_sage_r52,
        color = "#676767",
        weight = 1.5,
        smoothFactor = 0.5,
        opacity = 1,
        fillOpacity = 0,
        dashArray = 5,
        fillColor = "#676767",
        popup = ~paste0("SAGE : ",nom_sage)
      ) %>%
      leaflet::addLegend(
        position = "bottomright",
        pal = factpal_bassin_versant,
        values = ~classe,
        title = "Pression azot\u00e9e globale </br> en kg d'azote par ha",
        opacity = 1
      ) %>%
      leaflet::fitBounds(
        min(cadre_carte_bassin_versant$lon),min(cadre_carte_bassin_versant$lat),
        max(cadre_carte_bassin_versant$lon),max(cadre_carte_bassin_versant$lat)
      ) %>%
      leaflet.extras::addFullscreenControl(position = "topright") %>%
      leafem::addHomeButton(group = "zoomer sur la r\u00e9gion", position = "bottomright", add = TRUE) %>%
      htmlwidgets::onRender(js_export_carte)
  }
  else{
    leaflet::leaflet(
      filtered,
      options = leaflet::leafletOptions(zoomControl = FALSE)
    ) %>%
      leaflet::addProviderTiles(provider = "CartoDB.Positron") %>%
      leaflet::addPolygons(
        data = n_region_exp_r52,
        group = "zoomer sur la r\u00e9gion",
        color = "black",
        weight = 1,
        smoothFactor = 0.5,
        opacity = 1.0,
        fillOpacity = 0
      ) %>%
      leaflet::addPolygons(
        data = n_departement_exp_r52,
        color = "black",
        weight = 1,
        smoothFactor = 0.5,
        opacity = 1.0,
        fillOpacity = 0
      ) %>%
      leaflet::addPolygons(
        data = n_sage_r52,
        color = "#676767",
        weight = 1.5,
        smoothFactor = 0.5,
        opacity = 1,
        fillOpacity = 0,
        dashArray = 5,
        fillColor = "#676767",
        popup = ~paste0("SAGE : ",nom_sage)
      ) %>%
      leaflet::addPolygons(
        data = filtered,
        fillColor = ~ factpal_bassin_versant(classe),
        weight = 3,
        opacity = 0.2,
        color = "white",
        dashArray = "3",
        fillOpacity = 0.8,
        label = labels,
        labelOptions = leaflet::labelOptions(
          style = list("font-weight" = "normal", padding = "3px 8px"),
          textsize = "10px",
          direction = "auto"
        )
      ) %>%
      leaflet::addLegend(
        position = "bottomright",
        pal = factpal_bassin_versant,
        values = ~classe,
        title = "Pression azot\u00e9e globale </br> en kg d'azote par ha",
        opacity = 1
      ) %>%
      leaflet::fitBounds(
        min(cadre_carte_bassin_versant$lon),min(cadre_carte_bassin_versant$lat),
        max(cadre_carte_bassin_versant$lon),max(cadre_carte_bassin_versant$lat)
      ) %>%
      leaflet.extras::addFullscreenControl(position = "topright") %>%
      leafem::addHomeButton(group = "zoomer sur la r\u00e9gion", position = "bottomright", add = TRUE) %>%
      htmlwidgets::onRender(js_export_carte)
  }


}

#' plot_bv_col_pa_fct
#'
#' @description Une fonction pour produire un diagramme colonnes des pressions azotées minérale et organique par bassin versant selon les campagnes culturales
#' @param nom_bv nom d'un bassin versant
#'
#' @return Un diagramme.
#'
#' @export
#' @importFrom dplyr filter
#' @importFrom forcats fct_rev
#' @importFrom highcharter hchart hcaes hc_plotOptions hc_colors hc_title hc_xAxis hc_yAxis hc_legend hc_exporting
plot_bv_col_pa_fct <- function(nom_bv){
  filtered <- bassin_versant_col_pa %>%
    dplyr::filter(nom_bassin_versant == nom_bv)
  titre_plot_bv <- unique(filtered$nom_bassin_versant)
  highcharter::hchart(
    filtered,
    "column",
    highcharter::hcaes(
      x = annee_declaration,
      y = valeur,
      group = forcats::fct_rev(pression)
    )
  ) %>%
    highcharter::hc_plotOptions(series = list(stacking = "valeur", showInLegend = TRUE)) %>%
    highcharter::hc_colors(rev(choix_couleur_col_pa)) %>%
    highcharter::hc_title(
      text = titre_plot_bv,
      align = "left",
      style = list(color = "black", useHTML = TRUE)
    ) %>%
    highcharter::hc_xAxis(title = list(text = "Campagne culturale")) %>%
    highcharter::hc_yAxis(title = list(text = "Pression azot\u00e9e (kg/ha)", min = 0, max = 200)) %>%
    highcharter::hc_legend(reversed = FALSE, enabled = TRUE) %>%
    highcharter::hc_exporting(
      enabled = TRUE,
      buttons = list(contextButton = list(menuItems = c("downloadPNG", "downloadJPEG", "downloadPDF", "downloadCSV" )))
    )
}




