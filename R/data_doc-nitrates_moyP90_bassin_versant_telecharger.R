#' nitrates_moyP90_bassin_versant_telecharger
#'
#' données à télécharger des moyennes des  percentiles 90 par bassin versant par année.
#'
#' @format A data frame with 5937 rows and 8 variables:
#' \describe{
  #'   \item{ annee }{  numeric }
  #'   \item{ code_nature_eau }{  factor }
  #'   \item{ code_sage }{  character }
  #'   \item{ nom_sage }{  character }
  #'   \item{ code_bassin_versant }{  character }
  #'   \item{ nom_bassin_versant }{  character }
  #'   \item{ moyP90 }{  numeric }
  #'   \item{ classe }{  character }
  #' }
  #' @source DREAL
  "nitrates_moyP90_bassin_versant_telecharger"
