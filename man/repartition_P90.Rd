% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data_doc-repartition_P90.R
\docType{data}
\name{repartition_P90}
\alias{repartition_P90}
\title{repartition_P90}
\format{
A data frame with 11750 rows and 8 variables:
\describe{
  \item{ code_nature_eau }{  factor }
  \item{ annee }{  numeric }
  \item{ classe }{  factor }
  \item{ nbre_station_par_classe }{  integer }
  \item{ pourc }{  numeric }
  \item{ nbre_station_total }{  integer }
  \item{ nom_sage }{  character }
  \item{ nom_bassin_versant }{  character }
}
}
\source{
DREAL
}
\usage{
repartition_P90
}
\description{
répartition du nombre de stations par classe de percentiles 90 au niveau régional ou par SAGE ou par bassin versant, par annee.
}
\keyword{datasets}
